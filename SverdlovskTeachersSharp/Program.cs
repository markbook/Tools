﻿using System;
using AngleSharp.Dom;
using AngleSharp;
using AngleSharp.Html.Dom;
using System.Net;
using System.IO;
using System.Text.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Text;

namespace SverdlovskTeachers
{
	class Person
	{
		public string Name { get; set; }
		public string Email { get; set; }
		public string Url { get; set; }
		public string Img { get; set; }
		public string Position { get; set; }
		public string Phone { get; set; }
		public string Disciplines { get; set; }
		public string EduSpheres { get; set; }
		public int? Exp { get; set; }
		public Dictionary<string, string> AdditionalData { get; set; } = new Dictionary<string, string>();
	}
	class Program
	{
		const int maxThreadCount = 4;
		static volatile int currentThreadCount = 0;
		static readonly List<Person> People = new();
		static Random r = new Random();
		static void Main()
		{
			Task.Run(Start).Wait();
			Console.Read();
		}
		static async void Start()
		{
			var path = Path.Join(Directory.GetCurrentDirectory(), "..", "data", "urls.json");
			var schoolUrls = JsonConvert.DeserializeObject<string[]>(File.ReadAllText(path)).Select(x => x.Replace("https", "http")).Distinct();
			File.WriteAllText(path, JsonConvert.SerializeObject(schoolUrls));

			List<Person> people = new();
			foreach (var url in schoolUrls)
			{
				using WebClient webClient = new();
				webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
				var uri = new Uri(url);
				string html = webClient.DownloadString(uri);
				var context = BrowsingContext.New(Configuration.Default);
				var document = await context.OpenAsync(req => req.Content(html));
				Task<List<Person>> taskToRun;
				// if (document.Body.Id == "body")
				// {
				// 	// taskToRun = Task.Run(() => GrabSite(context, url + "/about/workersItem/{id}", OldSiteSelector));
				// }
				if (document.Body.ClassName == "page")
				{
					taskToRun = Task.Run(() => GrabSite(context, url + "/org-info/employee-card?id={id}", ModernSiteSelector));
					currentThreadCount++;
				}
				else continue;
				_ = taskToRun.ContinueWith(x =>
				{
					Console.WriteLine("Для {0} поулчено пользователей: {1}", url, x.Result.Count);
					People.AddRange(x.Result);
					var data = JsonConvert.SerializeObject(People, new JsonSerializerSettings
					{
						ContractResolver = new DefaultContractResolver()
						{
							NamingStrategy = new CamelCaseNamingStrategy()
						},
					});
					var path = Path.Join(Directory.GetCurrentDirectory(), "..", "data", "ekb_school_teachers.json");
					File.WriteAllText(path, data, Encoding.UTF8);
					currentThreadCount--;
				});
				while (currentThreadCount == maxThreadCount)
				{
					await Task.Delay(1);
				}
			}

		}
		static Uri BuildUri(string url, int id)
		{
			return new Uri(url.Replace("{id}", id.ToString()));
		}
		static async Task<List<Person>> GrabSite(IBrowsingContext context, string url, Func<IDocument, Uri, Person> selectorFunc)
		{
			int id = 1;
			var ppl = new List<Person>();
			using WebClient webClient = new();
			int emptyPagesCount = 0;
			while (id < 200)
			{
				if (emptyPagesCount > 4) break;
				var uri = BuildUri(url, id);
				var document = await context.OpenAsync(req => req.Content(webClient.DownloadString(uri)).Address(uri));
				id++;
				if (document.Body.ChildElementCount == 0)
				{
					emptyPagesCount++;
					continue;
				}
				var person = selectorFunc(document, uri);
				Console.WriteLine($"> {uri.Host.Split('.')[0]} {person.Position} {person.Name} {person.Email} {person.Phone}");
				ppl.Add(person);
				await Task.Delay(400);
			}
			return ppl;
		}
		static Person ModernSiteSelector(IDocument document, Uri uri)
		{
			var person = new Person();
			person.Url = uri.AbsolutePath;
			person.Name = document.QuerySelector("div.page-caption:nth-child(2) > h1:nth-child(1)").TextContent.Trim();
			person.Position = document.QuerySelector("p.post").TextContent.Trim();
			person.Img = document.QuerySelector<IHtmlImageElement>(".img > img:nth-child(1)")?.Source;
			var dynTables = document.GetElementsByClassName("key-val-table").ToList();
			foreach (var table in dynTables)
			{
				foreach (var row in table.Children)
				{
					string rowName = row.FirstElementChild.TextContent.Trim();
					string rowValue = row.LastElementChild.TextContent.Trim().Replace("\n", "").Replace("\t", "").Replace("\r", "");
					if (rowName == "E-mail") person.Email = rowValue;
					else if (rowName == "Телефон") person.Phone = rowValue;
					else if (rowName == "Преподаваемые дисциплины") person.Disciplines = rowValue;
					else if (rowName == "Направления подготовки")
					{
						person.EduSpheres = string.Join(" ", row.LastElementChild.Children.Select(x => x.TextContent));
					}
					else if (rowName == "Стаж работы по специальности (полных лет)")
					{
						if (int.TryParse(rowValue, out int exp))
							person.Exp = exp;
					}
					else
					{
						if (!person.AdditionalData.ContainsKey(rowName))
						{
							person.AdditionalData.Add(rowName, rowValue);
						}
					}
				}
			}
			return person;
		}
		// static Person OldSiteSelector(IDocument document, string url)
		// {
		// 	var name = document.All
		// 	.Where(x => x.ClassName == "caption")
		// 	.Where(x => x.GetAttribute("itemprop") == "fio").FirstOrDefault()?.Text();
		// 	var email = document.All.Where(x => x.TagName == "td").Where(x => x.ParentElement.FirstElementChild.Text() == "E-mail")
		// 	var phone = document.QuerySelector("table.contacts-info:nth-child(3) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2)").Text();
		// 	if (email == "Нет") email = null;
		// 	if (phone == "Нет") phone = null;
		// 	var img = document.QuerySelector<IHtmlImageElement>(".img > img:nth-child(1)");
		// 	var pos = document.QuerySelector("p.post").Text();
		// 	var disciplines = document.QuerySelector("table.contacts-info:nth-child(4) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2)").Text();
		// 	bool expValid = int.TryParse(document.QuerySelector("table.contacts-info:nth-child(4) > tbody:nth-child(1) > tr:nth-child(7) > td:nth-child(2)").Text(), out int exp);
		// 	var person = new Person(name, email, url, img.Source, pos, phone, disciplines, exp);
		// 	return person;
		// }
	}
}

