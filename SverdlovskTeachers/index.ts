import jsdom from "jsdom";
import fetch from "./node-fetch";
import fs from "fs";
import path from "path";
import { getElementByAttributeValue, sleep } from "../helpers";
const { JSDOM } = jsdom;
const mode: "one_page" | "multi_page" = "multi_page";
type Person = {
  name: string;
  email?: string;
  url: string;
  img?: string;
  position?: string;
  telephone?: string;
  disciplines?: string;
  exp?: number;
};
const fData = fs.readFileSync(path.resolve("..", "data", "urls.json"), "utf8");
const urls = JSON.parse(fData) as string[];
console.log("Всего сайтов: ", urls.length);

const persons: Person[] = [];
urls.forEach(async (url) => {
  const res = await fetch(url).then((r) => r.text());
  const { window } = new JSDOM(res);

  if (window.document.body.id === "body") {
    res = await fetch(url + "/about/workersItem/").then((r) => r.text());
    if (mode === "multi_page") {
      persons.push(...handleOldSiteMultiPage());
    } else {
      persons.push(...handleOldSiteOnePage());
    }
  } else {
    if (mode === "multi_page") {
      persons.push(...handleNewSiteMultiPage());
    } else {
      persons.push(...handleNewSiteOnePage());
    }
  }
  console.log(`С сайта ${url} были взяты емэйлы: ${persons.length}`);
  await sleep(1000 + Math.random() * 1000);
});

function handleNewSiteMultiPage(doc: Document, url: string) {
  const persons: Person[] = [];
}
function handleNewSiteOnePage(doc: Document, url: string) {}

function handleOldSiteOnePage(doc: Document, url: string) {
  const persons: Person[] = [];
  const workerCards = Array.from(doc.getElementsByClassName("workers-item"));
  workerCards.forEach((card) => {
    const itemContent = card.getElementsByClassName("item-content").item(0);
    const name = getElementByAttributeValue(itemContent, "p", "itemprop", "fio")
      ?.innerText;
    const position = getElementByAttributeValue(
      itemContent,
      "p",
      "itemprop",
      "Post"
    )?.innerText;
    const telephone = getElementByAttributeValue(
      itemContent,
      "p",
      "itemprop",
      "Telephone"
    )?.innerText;
    const email = getElementByAttributeValue(
      itemContent,
      "p",
      "itemprop",
      "e-mail"
    )
      ?.getElementsByTagName("a")
      .item(0).innerText;
    const imgUrl = document.getElementsByTagName("img").item(0).src;
    const exp = document.getElementsByTagName("img").item(0).src;
    persons.push({
      name,
      position,
      telephone,
      email,
      url,
      img: imgUrl,
    });
  });
  return persons;
}
fs.writeFileSync(
  path.resolve(".", `teachers-${new Date().toLocaleTimeString()}.txt`),
  JSON.stringify(persons)
);
