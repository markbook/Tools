export function getElementByAttributeValue(
  where: Element | Document,
  el: keyof HTMLElementTagNameMap,
  attr: string,
  attrValue: string
) {
  return Array.from(where.getElementsByTagName(el)).filter(
    (x) => x.getAttribute(attr) === attrValue
  )[0];
}
// type ElementShape = {
// 	id?: string;
// 	class?: string;
// 	attribute?: string;
// 	innerHTMLContains?: string;
// 	attrValue?: string;
// }
// export function findEl({attribute, class,id,innerHTMLContains}: ElementShape) {
// 	if(id) return
// }
export function sleep(ms: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
