﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Smtp;
using MimeKit;
using Newtonsoft.Json;

namespace EmailClient
{
	class Program
	{
		static void Main(string[] args)
		{
			string configPath = Path.Join(Directory.GetCurrentDirectory(), "config.json");
			var config = JsonConvert.DeserializeObject<ConnectionConfiguration>(File.ReadAllText(configPath));
			var imapConfig = config.ImapConfiguration;

			var smtpConfig = config.SmtpConfiguration;
			var myAddress = MailboxAddress.Parse(config.EMail);
			// bool smtpAuthenticated = false;
			// bool imapAuthenticated = false;


			var message = new MimeMessage();
			message.Subject = config.Subject;
			message.Sender = myAddress;

			var bb = new BodyBuilder();
			bb.TextBody = config.TextBody;
			bb.HtmlBody = File.ReadAllText(config.HtmlBodyFile).Trim();
			message.Body = bb.ToMessageBody();
			message.From.Add(new MailboxAddress("Платформа «Маркбук»", "welcome@маркбук.рф"));
			Console.WriteLine("Письмо загружено \"{0}\" загружено", message.Subject);
			Console.WriteLine("Введите список получателей");
			var receivers = File.ReadAllLines(config.RecepientsFilePath).Select(x => MailboxAddress.Parse(x)).ToList();
			Console.WriteLine(@"Введено {0} поулчателей. Все верно? (y\n)", receivers.Count);

			if (Console.ReadLine() == "y")
			{
				// using var imap = new ImapClient();
				// imap.Authenticated += (e, b) =>
				// {
				// 	imapAuthenticated = true;
				// 	sentFolder = imap.GetFolder(new FolderNamespace('.', "Sent"));
				// 	Console.WriteLine("Успешно подключен Imap.");
				// };
				// imap.Connect(imapConfig.Server, imapConfig.Port, true);
				// imap.Authenticate(Encoding.UTF8, imapConfig.UserName, imapConfig.Password);
				using var smtp = new SmtpClient();
				// smtp.Authenticated += (e, b) =>
				// {
				// 	smtpAuthenticated = true;
				// 	Console.WriteLine("Успешно подключен Smtp.");
				// };
				smtp.Connect(smtpConfig.Server, smtpConfig.Port);
				smtp.Authenticate(Encoding.UTF8, smtpConfig.UserName, smtpConfig.Password);


				// Console.WriteLine("Ожидание аутентификации...");
				// bool authed = !smtpAuthenticated && !imapAuthenticated;
				// if (authed)
				// {
				// 	Console.WriteLine("Ожидание аутентификации...");
				// 	while (authed)
				// 	{
				// 		Thread.Sleep(500);
				// 		Console.Write(".");
				// 	}
				// }
				foreach (var rec in receivers)
				{
					message.To.Clear();
					message.To.Add(rec);
					smtp.Send(message);
					Thread.Sleep(200);
				}
				smtp.Disconnect(true);
				File.WriteAllText(Path.Join(Directory.GetCurrentDirectory(), "sent " + DateTime.Now.ToString().Replace(":", "_") + ".txt"), string.Join(Environment.NewLine, receivers));
			}
			Console.WriteLine("Нажмите Esc чтобы выйти");
			while (true)
			{
				if (Console.KeyAvailable)
				{
					if (Console.ReadKey().Key == ConsoleKey.Escape)
					{
						Console.WriteLine("Работа завершается");
						break;
					}
				}
				Thread.Sleep(100);
			}
		}
	}
}
