namespace EmailClient
{
	public class ConnectionConfiguration
	{
		public string EMail { get; set; }

		public ImapConfiguration ImapConfiguration { get; set; }
		public SmtpConfiguration SmtpConfiguration { get; set; }
		public string Subject { get; set; }
		public string HtmlBodyFile { get; set; }
		public string TextBody { get; set; }
		public string RecepientsFilePath { get; set; }
	}
	public abstract class MailServerConfigurationBase
	{
		public string Server { get; set; }
		public int Port { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
	}
	public class ImapConfiguration : MailServerConfigurationBase
	{

	}
	public class SmtpConfiguration : MailServerConfigurationBase
	{

	}
}