import jsdom from "jsdom";
import fetch from "node-fetch";
import fs from "fs";
import path from "path";
const { JSDOM } = jsdom;
const alphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";

var result = "";
alphabet.split("").forEach((word, i, arr) => {
  setTimeout(async () => {
    const url = `https://urfu.ru/ru/about/personal-pages/Personal/index/?tx_urfupersonal_personal[alpha]=${encodeURI(
      word
    )}`;
    const res = await fetch(url);
    const { window } = new JSDOM(await res.text());
    const persons = window.document.getElementsByClassName("staff-block");
    Array.from(persons).forEach((personBlock) => {
      const email = personBlock.getElementsByTagName("a").item(1)?.innerHTML;
      if (email && result.indexOf(email) < 0) {
        result += email + "\n";
      }
    });
    if (i === arr.length - 1) {
      end();
    }
  }, 1500 * i);
});
function end() {
  fs.writeFileSync(path.resolve("..", "data", "emails.txt"), result);
}
